# Larastrap

Larastrap is an opinionated set of Bootstrap 5 components for Laravel.

Find all the documentation on https://larastrap.madbob.org/

## Install

```
composer require madbob/larastrap
php artisan vendor:publish --tag=config
```

Remeber to include the Bootstrap 5 assets (both CSS and JS) in your pages!

## License

This code is free software, licensed under the MIT License. See the LICENSE.txt file for more details.

Copyright (C) 2021 Roberto Guido info@madbob.org
