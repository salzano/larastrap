<x-larastrap::field :params="$params['field_params']">
    @if(filled($params['textappend']))
    <div class="input-group {{ $params['error_handling'] ? 'has-validation' : '' }}" {{ $params['hidden'] ? 'hidden' : '' }}>
    @endif

        <input id="{{ $params['id'] }}" type="{{ $inputtype }}" class="{{ $params['generated_class'] }}" name="{{ $params['actualname'] }}" value="{{ $params['value'] }}" {!! $params['serialized_attributes'] !!}>

    @if(filled($params['textappend']))
        <span class="input-group-text">{!! $params['textappend'] !!}</span>
    </div>
    @endif

    @if($params['error_handling'])
        @error($params['actualname'], $params['error_bag'])
            <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    @endif
</x-larastrap::field>
