@if($params['appendNodes'] && empty($params['appendNodes']) == false)
	@foreach($params['appendNodes'] as $node => $pars)
		<x-dynamic-component :component="$node" :params="$pars" />
	@endforeach
@endif
