<form id="{{ $params['id'] }}" class="{{ $params['generated_class'] }}" {!! $params['serialized_attributes'] !!}>
    @if($params['method'] != 'GET')
        @csrf

        @if($params['real_method'] != 'POST' && $params['real_method'] != 'GET')
            @method($params['real_method'])
        @endif
    @endif

    {{ $slot }}
    @include('larastrap::appended_nodes', ['params' => $params])

    @if(!empty($params['buttons']))
        <div class="col-12 text-{{ $params['buttons_align'] }}">
            @foreach($params['buttons'] as $btn)
                @php
                    $type = $btn['element'] ?? 'larastrap::button';
                    unset($btn['element']);
                @endphp
                <x-dynamic-component :component="$type" :params="$btn" />
            @endforeach
        </div>
    @endif
</form>
