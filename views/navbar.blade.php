<nav class="{{ $params['generated_class'] }}">
    <div class="container-fluid">
        @if(!empty($params['title']))
            <a class="navbar-brand" href="{{ $params['title_link'] }}">{!! $params['title'] !!}</a>
        @endif

        @if($params['collapse'] != 'never')
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#{{ $params['id'] }}" aria-label="Toggle Menu">
                <span class="navbar-toggler-icon"></span>
            </button>
        @endif

        <div class="collapse navbar-collapse position-relative" id="{{ $params['id'] }}">
            <ul class="navbar-nav">
                @include('larastrap::navbarlist', ['items' => $params['options']])
            </ul>

            @if(!empty($params['end_options']))
                <ul class="navbar-nav position-absolute end-0">
                    @include('larastrap::navbarlist', ['items' => $params['end_options']])
                </ul>
            @endif
        </div>
    </div>
</nav>
