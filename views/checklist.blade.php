<x-larastrap::field :params="$params['field_params']" :classes="$params['generated_class']">
    @foreach($params['options'] as $option_value => $option)
        @php
            $option = $parseOption($option, $params);
        @endphp

        <div class="form-check" {{ $option->hidden ? 'hidden' : '' }}>
            <input class="form-check-input" type="checkbox" name="{{ $params['actualname'] }}" id="{{ $option->id }}" value="{{ $option_value }}" {{ in_array($option_value, $params['value']) ? 'checked' : '' }}>
            <label class="{{ $option->serialized_label_classes }}" for="{{ $option->id }}" {!! $option->serialized_label_attributes !!}>{{ $option->label }}</label>
        </div>
    @endforeach

    @if($params['error_handling'])
        @error($params['name'], $params['error_bag'])
            <div class="invalid-feedback" style="display:block">{{ $message }}</div>
        @enderror
    @endif
</x-larastrap::field>
