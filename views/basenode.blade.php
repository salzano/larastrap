<{{ $params['node'] }} class="{{ $params['generated_class'] }}" {!! $params['serialized_attributes'] !!}>{{ $slot }}</{{ $params['node'] }}>
