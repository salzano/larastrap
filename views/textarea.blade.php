<x-larastrap::field :params="$params['field_params']">
    <textarea id="{{ $params['id'] }}" class="{{ $params['generated_class'] }}" name="{{ $params['actualname'] }}" {!! $params['serialized_attributes'] !!}>{!! $params['value'] !!}</textarea>

    @if($params['error_handling'])
        @error($params['actualname'], $params['error_bag'])
            <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    @endif
</x-larastrap::field>
