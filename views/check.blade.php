<x-larastrap::field :params="$params['field_params']">
    <div class="form-check {{ $params['switch'] ? 'form-switch' : '' }} mt-1">
        <input id="{{ $params['id'] }}" type="checkbox" class="{{ $params['generated_class'] }}" name="{{ $params['actualname'] }}" value="{{ $params['value'] }}" {!! $params['serialized_attributes'] !!}>
    </div>
</x-larastrap::field>
