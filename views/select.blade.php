<x-larastrap::field :params="$params['field_params']">
    @if($params['readonly'])
        @php
            if (is_array($params['value'])) {
                $static_value = [];

                foreach($params['value'] as $v) {
                    $op = $params['options'][$v] ?? null;
                    if ($op) {
                        $static_value[] = $op;
                    }
                }

                $static_value = join(', ', $static_value);
            }
            else {
                $static_value = $params['options'][$params['value']] ?? '';
            }
        @endphp

        <x-larastrap::text squeeze="true" readonly disabled :value="$static_value" />
    @else
        <select class="{{ $params['generated_class'] }}" name="{{ $params['actualname'] }}" {!! $params['serialized_attributes'] !!}>
            {!! $unrollOptions($params['options'], $params['value']) !!}
        </select>

        @if($params['error_handling'])
            @error($params['actualname'], $params['error_bag'])
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        @endif
    @endif
</x-larastrap::field>
