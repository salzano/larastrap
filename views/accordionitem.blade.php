<div class="{{ $params['generated_class'] }}" {!! $params['serialized_attributes'] !!}>
    <h2 class="accordion-header" id="head-{{ $params['id'] }}">
        <button class="accordion-button {{ $params['active'] ? '' : 'collapsed' }}" type="button" data-bs-toggle="collapse" data-bs-target="#{{ $params['id'] }}" aria-expanded="{{ $params['active'] ? 'true' : 'false' }}" aria-controls="{{ $params['id'] }}">
            {!! $params['label'] !!}
        </button>
    </h2>
    <div id="{{ $params['id'] }}" class="accordion-collapse collapse {{ $params['active'] ? 'show' : '' }}" aria-labelledby="head-{{ $params['id'] }}" {!! $params['parent_link'] !!}>
        <div class="accordion-body">
            {{ $slot }}
            @include('larastrap::appended_nodes', ['params' => $params])
        </div>
    </div>
</div>
