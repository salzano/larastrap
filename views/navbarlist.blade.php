@foreach($items as $label => $meta)
    @if(!empty($meta['children']))
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle {{ $meta['active'] ? 'active' : '' }}" href="#" role="button" data-bs-toggle="dropdown">{!! $label !!}</a>
            <ul class="dropdown-menu">
                @foreach($meta['children'] as $label => $submeta)
                    <li><a class="dropdown-item {{ $submeta['active'] ? 'active' : '' }}" href="{{ $submeta['url'] }}" {!! $submeta['serialized_attributes'] !!}>{!! $label !!}</a></li>
                @endforeach
            </ul>
        </li>
    @else
        <li class="nav-item">
            <a class="nav-link {{ $meta['active'] ? 'active' : '' }}" href="{{ $meta['url'] }}" {!! $meta['serialized_attributes'] !!}>{!! $label !!}</a>
        </li>
    @endif
@endforeach
