<div class="{{ $params['generated_class'] }}" id="{{ $params['id'] }}" tabindex="-1" aria-hidden="true" {!! $params['serialized_attributes'] !!}>
    <div class="modal-dialog {{ $params['generated_class_dialog'] }}">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ $params['title'] }}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
            </div>

            <div class="modal-body">
                {{ $slot }}
                @include('larastrap::appended_nodes', ['params' => $params])
            </div>

            @if(!empty($params['buttons'] ?? []))
                <div class="modal-footer">
                    @foreach($params['buttons'] as $btn)
                        <x-larastrap::button :params="$btn" />
                    @endforeach
                </div>
            @endif
        </div>
    </div>
</div>
