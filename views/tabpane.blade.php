<div class="{{ $params['generated_class'] }}" id="{{ $params['id'] }}" role="tabpanel">
    {{ $slot }}
    @include('larastrap::appended_nodes', ['params' => $params])
</div>
