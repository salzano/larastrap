<div class="{{ $params['generated_class'] }}" role="group" {!! $params['serialized_attributes'] !!}>
    {{ $slot }}
    @include('larastrap::appended_nodes', ['params' => $params])
</div>
