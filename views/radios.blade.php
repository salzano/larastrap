<x-larastrap::field :params="$params['field_params']">
    @php
        $params['value'] = \Illuminate\Support\Arr::wrap($params['value']);
    @endphp

    <x-larastrap::btngroup :id="$params['id']" :classes="$params['generated_class']" :attributes="$params['attributes']">
        @foreach($params['options'] as $option_value => $option)
            @php
                $option = $parseOption($option, $params);
            @endphp

            <input type="radio" class="btn-check" name="{{ $params['actualname'] }}" id="{{ $option->id }}" autocomplete="off" value="{{ $option_value }}" {{ in_array($option_value, $params['value']) ? 'checked' : '' }}>

            @if($option->hidden == false)
                <label class="{{ $option->serialized_button_classes }}" for="{{ $option->id }}" {!! $option->serialized_button_attributes !!}>{{ $option->label }}</label>
            @endif
        @endforeach
    </x-larastrap::btngroup>
</x-larastrap::field>
