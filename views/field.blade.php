<div class="{{ $params['generated_class'] }}">
    <label for="{{ $params['id'] }}" class="{{ join(' ', $params['label_class']) }}">
        {!! $params['label'] !!}

        @if(filled($params['pophelp']))
            <x-larastrap::pophelp :text="$params['pophelp']" />
        @endif
    </label>

    {!! $params['input_wrap_tag_start'] !!}

    {{ $slot }}

    @if(filled($params['help']))
        <div class="form-text">
            {!! html_entity_decode($params['help']) !!}
        </div>
    @endif

    {!! $params['input_wrap_tag_end'] !!}

    @include('larastrap::appended_nodes', ['params' => $params])
</div>
