<x-larastrap::field :params="$params['field_params']">
    @foreach($params['options'] as $option_value => $option)
        @php
            $option = $parseOption($option, $params);
        @endphp

        <div class="form-check" {{ $option->hidden ? 'hidden' : '' }}>
            <input class="form-check-input" type="radio" name="{{ $params['actualname'] }}" id="{{ $option->id }}" value="{{ $option_value }}"  {{ $params['value'] == $option_value ? 'checked' : '' }}>
            <label class="{{ $option->serialized_label_classes }}" for="{{ $option->id }}" {!! $option->serialized_label_attributes !!}>{{ $option->label }}</label>
        </div>
    @endforeach
</x-larastrap::field>
