<?php

$active_index = $params['active'] ?? 0;
$index = 0;

?>

<ul class="{{ $params['generated_class'] }}" id="{{ $params['id'] }}" role="tablist">
    @foreach($params['options'] as $target => $button)
        <li class="nav-item" role="presentation">
            @if($params['use_anchors'])
                <x-larastrap::link :params="$button" role="tab" data-bs-toggle="tab" data-bs-target="#{{ $target }}" />
            @else
                <x-larastrap::button :params="$button" role="tab" data-bs-toggle="tab" data-bs-target="#{{ $target }}" />
            @endif
        </li>
    @endforeach
</ul>

<div class="tab-content" id="{{ $params['id'] }}_contents">
    {{ $slot }}
    @include('larastrap::appended_nodes', ['params' => $params])
</div>
