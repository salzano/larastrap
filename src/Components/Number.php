<?php

namespace MadBob\Larastrap\Components;

class Number extends Input
{
    public static function parameters()
    {
        return array_merge(parent::parameters(), [
            'step' => (object) [
                'type' => 'integer',
                'default' => 1,
            ],
            'min' => (object) [
                'type' => 'integer',
                'default' => PHP_INT_MIN,
            ],
            'max' => (object) [
                'type' => 'integer',
                'default' => PHP_INT_MAX,
            ],
        ]);
    }

    protected function serializableAttributes()
    {
        $ret = parent::serializableAttributes();
        $ret->literals = array_merge($ret->literals, ['step', 'min', 'max']);
        return $ret;
    }

    protected function inputType()
    {
        return 'number';
    }
}
