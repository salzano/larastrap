<?php

namespace MadBob\Larastrap\Components;

class Url extends Input
{
    protected function inputType()
    {
        return 'url';
    }
}
