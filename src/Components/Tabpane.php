<?php

namespace MadBob\Larastrap\Components;

use MadBob\Larastrap\Base\Container;

class Tabpane extends Container
{
    public static function parameters()
    {
        return array_merge(parent::parameters(), [
            'active' => (object) [
                'type' => 'boolean',
                'default' => false,
            ],
            'label' => (object) [
                'type' => 'string',
                'default' => '',
            ],
            'tlabel' => (object) [
                'type' => 'string',
                'translates' => 'label',
                'default' => '',
            ],
            'label_html' => (object) [
                'type' => 'html_version',
                'default' => '',
                'to' => 'label',
            ],
            'button_classes' => (object) [
                'type' => 'string_array',
                'default' => [],
            ],
            'button_attributes' => (object) [
                'type' => 'array',
                'default' => [],
            ],
        ]);
    }

    protected function processParams($params)
    {
        $params = parent::processParams($params);
        $params['button_classes'][] = 'nav-link';
        return $params;
    }

    public function activeParams($params)
    {
        $params['classes'][] = 'show';
        $params['classes'][] = 'active';
        $params['button_classes'][] = 'active';
        return $params;
    }

    public function makeButton($params)
    {
        $ret = [
            'label' => html_entity_decode($params['label']),
            'override_classes' => $params['button_classes'],
            'attributes' => $params['button_attributes'],
        ];

        $ret['id'] = $params['id'] . '-' . md5($ret['label']);
        return $ret;
    }

    protected function finalProcess($params)
    {
        $params = $this->closestParent(Tabs::class)->addOption($this, $params);
        return parent::finalProcess($params);
    }

    protected function baseClass()
    {
        return 'tab-pane fade';
    }

    protected function localAttributes()
    {
        return array_merge(parent::localAttributes(), ['active', 'button_classes', 'button_attributes']);
    }
}
