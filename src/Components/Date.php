<?php

namespace MadBob\Larastrap\Components;

class Date extends Input
{
    protected function inputType()
    {
        return 'date';
    }

    protected function processParams($params)
    {
        $params = parent::processParams($params);

        /*
            Especially when handling dates as Carbon objects, in the final
            output are formatted with the time. Which breaks the HTML input
            date. So here the string is eventually broken down to keep only the
            date part
        */
        $value = (string) $params['value'] ?? '';
        if (strpos($value, ' ') !== false) {
            $params['value'] = explode(' ', $value)[0];
        }

        return $params;
    }
}
