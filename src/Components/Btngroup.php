<?php

namespace MadBob\Larastrap\Components;

use MadBob\Larastrap\Base\Container;

class Btngroup extends Container
{
    public static function parameters()
    {
        return array_merge(parent::parameters(), [
            'orientation' => (object) [
                'type' => 'enum:horizontal,vertical',
                'default' => 'horizontal',
            ],
        ]);
    }

    protected function baseClass()
    {
        return '';
    }

    protected function processParams($params)
    {
        $params = parent::processParams($params);

        switch($params['orientation']) {
            case 'horizontal':
                $css_class = 'btn-group';
                break;

            case 'vertical':
                $css_class = 'btn-group-vertical';
                break;
        }

        $params['classes'][] = $css_class;

        return $params;
    }
}
