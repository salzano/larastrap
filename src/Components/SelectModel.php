<?php

namespace MadBob\Larastrap\Components;

use MadBob\Larastrap\Base\TranslateModels;

class SelectModel extends Select
{
    use TranslateModels;

    public static function parameters()
    {
        return array_merge(parent::parameters(), self::translatingParameters());
    }

    public function templateName()
    {
        return 'select';
    }

    protected function processParams($params)
    {
        $params = parent::processParams($params);
        return $this->translateOptions($params);
    }
}
