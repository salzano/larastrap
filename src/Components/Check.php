<?php

namespace MadBob\Larastrap\Components;

use MadBob\Larastrap\Base\Commons;

class Check extends Input
{
    public static function parameters()
    {
        return array_merge(parent::parameters(), [
            'checked' => (object) [
                'type' => 'boolean',
                'default' => null,
            ],
            'switch' => (object) [
                'type' => 'boolean',
                'default' => false,
            ],
            'triggers_collapse' => (object) [
                'type' => 'string',
                'default' => '',
            ],
        ]);
    }

    protected function serializableAttributes()
    {
        $ret = parent::serializableAttributes();
        $ret->booleans = array_merge($ret->booleans, ['checked']);
        $ret->literals = array_merge($ret->literals, ['data-bs-toggle', 'data-bs-target']);
        return $ret;
    }

    protected function baseClass()
    {
        return 'form-check-input';
    }

    protected function inputType()
    {
        return 'checkbox';
    }

    public function templateName()
    {
        return 'check';
    }

    protected function processParams($params)
    {
        $params = parent::processParams($params);

        if ($params['checked'] === null && (is_bool($params['value']) || ($params['value'] === 0 || $params['value'] === 1))) {
            $params['checked'] = $params['value'];
        }

        return $params;
    }

    protected function finalProcess($params)
    {
        if (filled($params['triggers_collapse'])) {
            $params['data-bs-toggle'] = 'collapse';
            $params['data-bs-target'] = Commons::prefixId($params['triggers_collapse']);

            if ($params['checked']) {
                $id = Commons::unprefixId($params['triggers_collapse']);
                $this->getStack()->setStatusByID($id, ['open' => true]);
            }
        }

        return parent::finalProcess($params);
    }
}
