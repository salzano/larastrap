<?php

namespace MadBob\Larastrap\Components;

use MadBob\Larastrap\Base\Element;
use MadBob\Larastrap\Base\Commons;

class Button extends Element
{
    public static function parameters()
    {
        return array_merge(parent::parameters(), [
            'color' => (object) [
                'type' => 'string',
                'default' => 'primary',
            ],
            'label' => (object) [
                'type' => 'string',
                'default' => '',
            ],
            'tlabel' => (object) [
                'type' => 'string',
                'translates' => 'label',
                'default' => '',
            ],
            'prelabel' => (object) [
                'type' => 'string',
                'default' => '',
            ],
            'postlabel' => (object) [
                'type' => 'string',
                'default' => '',
            ],
            'size' => (object) [
                'type' => 'enum:sm,none,lg',
                'default' => 'none',
            ],
            'triggers_modal' => (object) [
                'type' => 'string',
                'default' => '',
            ],
            'triggers_collapse' => (object) [
                'type' => 'string',
                'default' => '',
            ],
        ]);
    }

    protected function baseClass()
    {
        return 'btn';
    }

    protected function serializableAttributes()
    {
        $ret = parent::serializableAttributes();
        $ret->literals = array_merge($ret->literals, ['data-bs-toggle', 'data-bs-target']);
        return $ret;
    }

    protected function processParams($params)
    {
        $params = parent::processParams($params);

        if (empty($params['override_classes'])) {
            $params['classes'][] = 'btn-' . $params['color'];

            if ($params['size'] != 'none') {
                $params['classes'][] = 'btn-' . $params['size'];
            }
        }

        if (filled($params['triggers_modal'])) {
            $params['data-bs-toggle'] = 'modal';
            $params['data-bs-target'] = Commons::prefixId($params['triggers_modal']);
        }

        if (filled($params['triggers_collapse'])) {
            $params['data-bs-toggle'] = 'collapse';
            $params['data-bs-target'] = Commons::prefixId($params['triggers_collapse']);
        }

        $params['label'] = trim(sprintf('%s%s%s', $params['prelabel'], $params['label'], $params['postlabel']));

        return $params;
    }
}
