<?php

namespace MadBob\Larastrap\Components;

use MadBob\Larastrap\Base\Container;

class Form extends Container
{
    public static function parameters()
    {
        return array_merge(parent::parameters(), [
            'formview' => (object) [
                'type' => 'enum:horizontal,vertical,inline,grid',
                'default' => 'horizontal',
            ],
            'label_width' => (object) [
                'type' => 'integer',
                'default' => 2,
            ],
            'input_width' => (object) [
                'type' => 'integer',
                'default' => 10,
            ],
            'method' => (object) [
                'type' => 'string',
                'default' => '',
            ],
            'action' => (object) [
                'type' => 'string',
                'default' => '',
            ],
            'baseaction' => (object) [
                'type' => 'string',
                'default' => '',
            ],
            'gutter' => (object) [
                'type' => 'integer',
                'default' => 3,
            ],
            'enctype' => (object) [
                'type' => 'string',
                'default' => '',
            ],
            'client_side_errors' => (object) [
                'type' => 'boolean',
                'default' => false,
            ],
            'error_handling' => (object) [
                'type' => 'boolean',
                'default' => true,
            ],
            'error_bag' => (object) [
                'type' => 'string',
                'default' => 'default',
            ],
            'buttons_align' => (object) [
                'type' => 'enum:start,center,end',
                'default' => 'end',
            ],
            'buttons' => (object) [
                'type' => 'array',
                'default' => [
                    ['color' => 'primary', 'label' => 'Save', 'attributes' => ['type' => 'submit']]
                ]
            ],
        ]);
    }

    protected function serializableAttributes()
    {
        return (object) [
            'literals' => ['method', 'action', 'enctype'],
            'booleans' => [],
        ];
    }

    protected function localAttributes()
    {
        return array_merge(parent::localAttributes(), ['buttons']);
    }

    protected function processParams($params)
    {
        $params = parent::processParams($params);
        $myclasses = [];

        if ($params['client_side_errors']) {
            $myclasses[] = 'needs-validation';
            $params['attributes'] = array_merge($params['attributes'] ?? [], ['novalidate' => true]);
        }

        switch($params['formview']) {
            case 'inline':
                $myclasses = ['row', 'row-cols-lg-auto', 'g-' . $params['gutter'], 'align-items-center'];
                break;
            case 'grid':
                $myclasses = ['row', 'g-' . $params['gutter']];
                break;
        }

        $params['classes'] = array_merge($params['classes'] ?? [], $myclasses);

        $method = strtoupper($params['method']);
        $params['real_method'] = $params['method'] = $method;
        if ($method != 'GET' && $method != 'POST') {
            $params['method'] = 'POST';
        }

        return $params;
    }

    protected function finalProcess($params)
    {
        $parent = $this->closestParent(Modal::class);
        if ($parent) {
            $my_buttons = [];

            foreach($params['buttons'] as $btn) {
                if (!isset($btn['form']) && !isset($btn['attributes']['form'])) {
                    $btn['attributes']['form'] = $params['id'];
                }

                $my_buttons[] = $btn;
            }

            $modal = $this->getStack()->getStatus($parent);
            $modal_buttons = array_merge($modal['buttons'], $my_buttons);
            $this->getStack()->updateStatus($parent, ['buttons' => $modal_buttons]);
            $params['buttons'] = [];
        }

        if (blank($params['action'])) {
            if (filled($params['baseaction'])) {
                $prefix = $params['baseaction'];

                if ($params['obj'] && is_object($params['obj']) && (is_a($params['obj'], \Illuminate\Database\Eloquent\Model::class) == false || $params['obj']->exists)) {
                    $params['action'] = route($prefix . '.update', $params['obj']->id);
                    $params['real_method'] = 'PUT';
                }
                else {
                    $params['action'] = route($prefix . '.store');
                }
            }
        }

        foreach($params['buttons'] as $k=>$v) {
            $type = $params['buttons'][$k]['type'] ?? 'button';
            $params['buttons'][$k]['type'] = sprintf('larastrap::%s', $type);
        }

        return parent::finalProcess($params);
    }
}
