<?php

namespace MadBob\Larastrap\Components;

class Color extends Input
{
    protected function inputType()
    {
        return 'color';
    }
}
