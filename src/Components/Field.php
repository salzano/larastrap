<?php

namespace MadBob\Larastrap\Components;

use MadBob\Larastrap\Base\Container;

class Field extends Container
{
    public static function parameters()
    {
        return array_merge(parent::parameters(), [
            'label' => (object) [
                'type' => 'string',
                'default' => '',
            ],
            'tlabel' => (object) [
                'type' => 'string',
                'translates' => 'label',
                'default' => '',
            ],
            'label_html' => (object) [
                'type' => 'html_version',
                'default' => '',
                'to' => 'label',
            ],
            'label_width' => (object) [
                'type' => 'integer|array',
                'default' => 2,
            ],
            'input_width' => (object) [
                'type' => 'integer|array',
                'default' => 10,
            ],
            'label_class' => (object) [
                'type' => 'string_array',
                'default' => [],
            ],
            'help' => (object) [
                'type' => 'string',
                'default' => null,
            ],
            'thelp' => (object) [
                'type' => 'string',
                'translates' => 'help',
                'default' => '',
            ],
            'pophelp' => (object) [
                'type' => 'string',
                'default' => null,
            ],
            'tpophelp' => (object) [
                'type' => 'string',
                'translates' => 'pophelp',
                'default' => '',
            ],
            'squeeze' => (object) [
                'type' => 'boolean',
                'default' => false,
            ],
        ]);
    }

    public function render()
    {
        return function (array $data) {
            $params = $this->generateParams(true);

            /*
                If the child input is "squeezed", the field has not to be
                displayed. In this case just the internal slot is returned.
                The attribute "squeeze" is set up to the parent in
                Commons::processParamsAsInput()
            */
            if ($params['squeeze'] == true) {
                $this->getStack()->popStatus();
                return (string) $data['slot'];
            }
            else {
                return parent::render()($data);
            }
        };
    }

    private function breakpointWidths($config)
    {
        if (is_array($config)) {
            $width_class = [];

            foreach($config as $breakpoint => $width) {
                if ($breakpoint == 'xs') {
                    $width_class[] = sprintf('col-%d', $width);
                }
                else {
                    $width_class[] = sprintf('col-%s-%d', $breakpoint, $width);
                }
            }

            $width_class = join(' ', $width_class);
        }
        else {
            $width_class = sprintf('col-%d', $config);
        }

        return $width_class;
    }

    protected function processParams($params)
    {
        $params = parent::processParams($params);

        $input_wrap_tag_start = '';
        $input_wrap_tag_end = '';

        $label_width_class = $this->breakpointWidths($params['label_width']);
        $input_width_class = $this->breakpointWidths($params['input_width']);
        $label_class = [];

        switch($params['formview'] ?? 'horizontal') {
            case 'horizontal':
                $main_wrap_class = 'row mb-3';
                $label_class = [$label_width_class, 'col-form-label'];
                $input_wrap_tag_start = '<div class="' . $input_width_class . '">';
                $input_wrap_tag_end = '</div>';
                break;

            case 'vertical':
                $main_wrap_class = 'mb-3';
                $label_class = ['form-label'];
                break;

            case 'grid':
                $main_wrap_class = $input_width_class;
                $label_class = ['form-label'];
                break;

            case 'inline':
                $main_wrap_class = 'col-12';
                $label_class = ['d-none'];
                break;
        }

        $params['classes'][] = $main_wrap_class;

        if (isset($params['label_class'])) {
            $params['label_class'] = array_merge($params['label_class'], $label_class);
        }
        else {
            $params['label_class'] = $label_class;
        }

        $params['input_wrap_tag_start'] = ' ' . $input_wrap_tag_start;
        $params['input_wrap_tag_end'] = ' ' . $input_wrap_tag_end;

        return $params;
    }
}
