<?php

namespace MadBob\Larastrap\Components;

class File extends Input
{
    protected function inputType()
    {
        return 'file';
    }

    protected function processParams($params)
    {
        $params = parent::processParams($params);

        $parent = $this->closestParent(Form::class);
        if ($parent) {
            $this->getStack()->updateStatus($parent, ['enctype' => 'multipart/form-data']);
        }

        return $params;
    }
}
