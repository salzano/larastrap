<?php

namespace MadBob\Larastrap\Components;

use MadBob\Larastrap\Base\Element;
use MadBob\Larastrap\Base\Commons;

abstract class Input extends Element
{
    public static function parameters()
    {
        return array_merge(Field::parameters(), [
            'name' => (object) [
                'type' => 'string',
                'default' => '',
            ],
            'nprefix' => (object) [
                'type' => 'string',
                'default' => '',
            ],
            'npostfix' => (object) [
                'type' => 'string',
                'default' => '',
            ],
            'value' => (object) [
                'type' => 'string',
                'default' => null,
            ],
            'required' => (object) [
                'type' => 'boolean',
                'default' => false,
            ],
            'disabled' => (object) [
                'type' => 'boolean',
                'default' => false,
            ],
            'readonly' => (object) [
                'type' => 'boolean',
                'default' => false,
            ],
            'asplaintext' => (object) [
                'type' => 'boolean',
                'default' => true,
            ],
            'placeholder' => (object) [
                'type' => 'string',
                'default' => null,
            ],
            'tplaceholder' => (object) [
                'type' => 'string',
                'translates' => 'placeholder',
                'default' => null,
            ],
            'textappend' => (object) [
                'type' => 'string',
                'default' => null,
            ],
            'ttextappend' => (object) [
                'type' => 'string',
                'translates' => 'textappend',
                'default' => '',
            ],
            'squeeze' => (object) [
                'type' => 'boolean',
                'default' => false,
            ],
            'error_handling' => (object) [
                'type' => 'boolean',
                'default' => true,
            ],
            'error_bag' => (object) [
                'type' => 'string',
                'default' => 'default',
            ],
        ]);
    }

    protected function serializableAttributes()
    {
        $ret = parent::serializableAttributes();
        $ret->literals = array_merge($ret->literals, ['placeholder']);
        $ret->booleans = array_merge($ret->booleans, ['required', 'disabled', 'readonly']);
        return $ret;
    }

    protected function fixedAttributes()
    {
        return [
            'inputtype' => $this->inputType(),
        ];
    }

    protected function baseClass()
    {
        return 'form-control';
    }

    public function templateName()
    {
        return 'input';
    }

    protected function processParams($params)
    {
        $params = parent::processParams($params);
        return Commons::processParamsAsInput($this, $params);
    }

    abstract protected function inputType();
}
