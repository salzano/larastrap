<?php

namespace MadBob\Larastrap\Components;

use MadBob\Larastrap\Base\Element;

class Pophelp extends Element
{
    public static function parameters()
    {
        return array_merge(parent::parameters(), [
            'text' => (object) [
                'type' => 'string',
                'default' => '',
            ],
            'ttext' => (object) [
                'type' => 'string',
                'translates' => 'text',
                'default' => '',
            ],
        ]);
    }

    protected function processParams($params)
    {
        $params = parent::processParams($params);

        if (empty($params['override_classes'])) {
            $params['classes'][] = 'badge';
            $params['classes'][] = 'rounded-pill';
            $params['classes'][] = 'bg-primary';
        }

        return $params;
    }
}
