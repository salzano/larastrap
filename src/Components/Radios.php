<?php

namespace MadBob\Larastrap\Components;

use MadBob\Larastrap\Base\Element;
use MadBob\Larastrap\Base\Commons;

class Radios extends Element
{
    public static function parameters()
    {
        return array_merge(Input::parameters(), [
            'color' => (object) [
                'type' => 'string',
                'default' => 'outline-primary',
            ],
            'options' => (object) [
                'type' => 'index_array',
                'default' => [],
            ],
            'button_classes' => (object) [
                'type' => 'string_array',
                'default' => [],
            ],
        ]);
    }

    protected function exposedMethods()
    {
        return ['parseOption'];
    }

    protected function processParams($params)
    {
        $params = parent::processParams($params);
        $params = Commons::processParamsAsInput($this, $params);

        if ($params['required'] && empty($params['value']) && count($params['options']) == 1) {
            $params['value'] = array_keys($params['options'])[0];
        }

        return $params;
    }

    protected function localAttributes()
    {
        return array_merge(parent::localAttributes(), ['button_classes']);
    }

    public function parseOption($option, $params)
    {
        $css_classes = [
            'btn',
            sprintf('btn-%s', $params['color']),
        ];

        if (is_string($option)) {
            $ret = (object) [
                'id' => '',
                'hidden' => false,
                'label' => $option,
                'disabled' => false,
                'classes' => [],
                'serialized_button_attributes' => '',
            ];
        }
        else {
            $ret = (object) [
                'id' => $option->id ?? false,
                'hidden' => $option->hidden ?? false,
                'label' => $option->label ?? '',
                'disabled' => $option->disabled ?? false,
                'classes' => $option->button_classes ?? [],
                'serialized_button_attributes' => Commons::serializeAttributes($option->button_attributes ?? []),
            ];
        }

        if ($ret->disabled) {
            $css_classes[] = 'disabled';
        }

        if (empty($ret->id)) {
            $ret->id = $params['id'] . '-' . md5($ret->label);
        }

        if (!empty($ret->classes)) {
            $css_classes = array_merge($css_classes, $ret->classes);
        }

        if (!empty($params['button_classes'])) {
            $css_classes = array_merge($css_classes, $params['button_classes']);
        }

        $ret->serialized_button_classes = join(' ', $css_classes);

        return $ret;
    }
}
