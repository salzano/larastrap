<?php

namespace MadBob\Larastrap\Components;

use Route;

use MadBob\Larastrap\Base\Element;
use MadBob\Larastrap\Base\Commons;

class Navbar extends Element
{
    public static function parameters()
    {
        return array_merge(parent::parameters(), [
            'title' => (object) [
                'type' => 'string',
                'default' => '',
            ],
            'title_link' => (object) [
                'type' => 'string',
                'default' => '',
            ],
            'color' => (object) [
                'type' => 'string',
                'default' => 'light',
            ],
            'collapse' => (object) [
                'type' => 'enum:never,md,lg,xl,xxl',
                'default' => 'lg',
            ],
            'options' => (object) [
                'type' => 'index_array',
                'default' => [],
            ],
            'end_options' => (object) [
                'type' => 'index_array',
                'default' => [],
            ],
        ]);
    }

    private function findActive(&$options)
    {
        foreach($options as &$meta) {
            $sub = $this->findActive($meta['children']);

            if ($sub || $meta['current_url']) {
                $meta['active'] = true;
                return true;
            }
        }

        return false;
    }

    private function formatOptions($current_path, $params, &$has_active)
    {
        $ret = [];

        foreach($params as $label => $meta) {
            $this_active = false;
            $item = [];

            if (is_string($meta)) {
                $url = $meta;
                $active = false;
                $children = [];
                $attributes = '';
            }
            else {
                if (isset($meta['url'])) {
                    $url = $meta['url'];
                }
                elseif (isset($meta['route'])) {
                    $url = route($meta['route']);
                }
                else {
                    $url = '#';
                }

                $active = $meta['active'] ?? false;
                $children = $this->formatOptions($current_path, $meta['children'] ?? [], $this_active);
                $attributes = Commons::serializeAttributes($meta['attributes'] ?? []);
            }

            $path = parse_url($url, PHP_URL_PATH);

            $ret[$label] = [
                'url' => $url,
                'current_url' => $current_path == $path,
                'active' => $active || $this_active,
                'children' => $children,
                'serialized_attributes' => $attributes,
            ];

            if ($active || $this_active) {
                $has_active = true;
            }
        }

        return $ret;
    }

    protected function processParams($params)
    {
        $params = parent::processParams($params);

        $current_route = Route::current();
        if ($current_route) {
            $current_path = '/' . $current_route->uri();
        }
        else {
            $current_path = '';
        }

        $has_active = false;
        $params['options'] = $this->formatOptions($current_path, $params['options'], $has_active);
        $params['end_options'] = $this->formatOptions($current_path, $params['end_options'], $has_active);

        if ($has_active == false) {
            if ($this->findActive($params['options']) == false) {
                $this->findActive($params['end_options']);
            }
        }

        $params['classes'][] = sprintf('navbar-%s', $params['color']);
        $params['classes'][] = sprintf('bg-%s', $params['color']);

        switch($params['collapse']) {
            case 'never':
                $params['classes'][] = 'navbar-expand';
                break;

            default:
                $params['classes'][] = sprintf('navbar-expand-%s', $params['collapse']);
                break;
        }

        return $params;
    }

    protected function baseClass()
    {
        return 'navbar';
    }
}
