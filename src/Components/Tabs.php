<?php

namespace MadBob\Larastrap\Components;

use MadBob\Larastrap\Base\Container;

class Tabs extends Container
{
    public static function parameters()
    {
        return array_merge(parent::parameters(), [
            'tabview' => (object) [
                'type' => 'string',
                'default' => 'tabs',
            ],
            'use_anchors' => (object) [
                'type' => 'boolean',
                'default' => false,
            ],
            'active' => (object) [
                'type' => 'integer',
                'default' => -1,
            ],
        ]);
    }

    protected function baseClass()
    {
        return 'nav';
    }

    protected function processParams($params)
    {
        $params = parent::processParams($params);

        switch($params['tabview']) {
            /*
                TODO: the vertical layout needs some work
            */
            case 'column':
                $css_class = 'flex-column';
                break;

            default:
                $css_class = sprintf('nav-%s', $params['tabview']);
                break;
        }

        $params['classes'][] = $css_class;

        $parent = $this->closestParent(Form::class);
        if ($parent) {
            $params['use_anchors'] = true;
        }

        return $params;
    }

    protected function finalProcess($params)
    {
        if (isset($params['inner_active'])) {
            $params['active'] = $params['inner_active'];
            unset($params['inner_active']);
        }

        return parent::finalProcess($params);
    }

    public function addOption($panel, $panel_params)
    {
        $params = $this->getStack()->getStatus($this);

        if (!isset($params['options'])) {
            $params['options'] = [];
        }

        $options_count = count($params['options']);

        /*
            Here we evaluate the activation status of the current panel, which
            may explicitely active or match the active index passed as a
            parameter to the parent tabs container
        */

        $make_active = false;

        if ($panel_params['active']) {
            $params['inner_active'] = $options_count;
            $make_active = true;
        }
        else {
            if ($params['active'] == $options_count) {
                $make_active = true;
            }
        }

        if ($make_active) {
            $panel_params = $panel->activeParams($panel_params);
        }

        $params['options'][$panel_params['id']] = $panel->makeButton($panel_params);
        $this->getStack()->updateStatus($this, $params);

        return $panel_params;
    }
}
