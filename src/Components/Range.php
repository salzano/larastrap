<?php

namespace MadBob\Larastrap\Components;

class Range extends Number
{
    protected function inputType()
    {
        return 'range';
    }

    protected function baseClass()
    {
        return 'form-range';
    }
}
