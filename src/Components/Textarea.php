<?php

namespace MadBob\Larastrap\Components;

use MadBob\Larastrap\Base\Element;
use MadBob\Larastrap\Base\Commons;

class Textarea extends Element
{
    public static function parameters()
    {
        return Input::parameters();
    }

    protected function serializableAttributes()
    {
        return (object) [
            'literals' => ['placeholder'],
            'booleans' => ['required', 'disabled', 'readonly']
        ];
    }

    protected function baseClass()
    {
        return 'form-control';
    }

    protected function processParams($params)
    {
        $params = parent::processParams($params);
        return Commons::processParamsAsInput($this, $params);
    }
}
