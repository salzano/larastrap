<?php

namespace MadBob\Larastrap\Components;

class Email extends Input
{
    protected function inputType()
    {
        return 'email';
    }
}
