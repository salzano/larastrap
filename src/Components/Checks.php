<?php

namespace MadBob\Larastrap\Components;

use MadBob\Larastrap\Base\Commons;

class Checks extends Radios
{
    protected function processParams($params)
    {
        /*
            This is to enforce handling options as an HTML array
        */
        if (empty($params['npostfix'])) {
            $params['npostfix'] = '[]';
        }

        $params = parent::processParams($params);
        $params = Commons::processParamsMultipleValues($params);

        return $params;
    }
}
