<?php

namespace MadBob\Larastrap\Components;

use Illuminate\Support\Str;

class Link extends Button
{
    public static function parameters()
    {
        return array_merge(parent::parameters(), [
            'href' => (object) [
                'type' => 'string',
                'default' => '#',
            ],
        ]);
    }

    protected function serializableAttributes()
    {
        $ret = parent::serializableAttributes();
        $ret->literals = array_merge($ret->literals, ['href']);
        return $ret;
    }
}
