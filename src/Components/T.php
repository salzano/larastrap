<?php

namespace MadBob\Larastrap\Components;

use Illuminate\Support\HtmlString;

use MadBob\Larastrap\Base\Container;

class T extends Container
{
    public static function parameters()
    {
        return array_merge(parent::parameters(), [
            'node' => (object) [
                'type' => 'string',
                'default' => 'span',
            ],
            'name' => (object) [
                'type' => 'string',
                'default' => '',
            ],
            'prelabel' => (object) [
                'type' => 'string',
                'default' => '',
            ],
            'postlabel' => (object) [
                'type' => 'string',
                'default' => '',
            ],
        ]);
    }

    protected function processParams($params)
    {
        $params = parent::processParams($params);

        $slot = $this->getSlot();
        if ($slot->isEmpty()) {
            $name = $params['name'] ?? null;
            if ($name) {
                $stack = app()->make('LarastrapStack');
                $value = $params['value'] ?? $stack->getLocalValue($name, $params) ?: $stack->getValue($name);
                if ($value) {
                    $value = new HtmlString(trim(sprintf('%s%s%s', $params['prelabel'], $value, $params['postlabel'])));
                    $this->setSlot($value);
                }
            }
        }

        return $params;
    }

    public function templateName()
    {
        return 'basenode';
    }
}
