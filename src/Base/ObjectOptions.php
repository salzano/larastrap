<?php

/*
    An essential issue with Blade Components is that attributes are HTML
    escaped, including Collections of Models (which become JSON strings).
    The only way to obtain the actual object is to explicitely grab them it in
    the constructor, and then inject it within the classic $params handling.
    This Trait is intended for all Components that may receive an "options"
    attribute, with a Collection inside
*/

namespace MadBob\Larastrap\Base;

use Illuminate\Database\Eloquent\Model;

trait ObjectOptions
{
    public $options;

    public function __construct($options = null)
    {
        $this->options = $options;
        parent::__construct();
    }

    public function fixOptions($params)
    {
        if ($this->options !== null) {
            $params['options'] = $this->options;
        }
        else {
            if (is_callable($params['options'])) {
                $params['options'] = $params['options']();
            }
        }

        return $params;
    }
}
