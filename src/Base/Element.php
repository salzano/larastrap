<?php

namespace MadBob\Larastrap\Base;

use Illuminate\View\Component;
use Illuminate\Support\Str;

abstract class Element extends Component
{
    private $unique_id = null;
    private $nodename = null;
    private $stack = null;

    protected $except = [
        'defaults',
    ];

    /*
        The main issue here is that it is not possible to access component's
        attributes until within the render() callback, but often parent's
        attributes are required to render his children (e.g. when formatting
        fields into a form with a custom "view" layout).
        The trick is to store informations about the hierarchy into the Stack,
        and process the parameters of parents when required (and when not
        processed yet) and within the render() context, when attributes are
        accessible.
    */
    private $parent;

    public function __construct()
    {
        $this->stack = $this->getStack();
        $this->parent = $this->stack->getCurrentContainer();
        $this->stack->pushStatus($this);
    }

    public function render()
    {
        return function (array $data) {
            if (is_a($this, Container::class)) {
                $this->setSlot($data['slot']);
            }

            $params = $this->generateParams(true);
            $this->getStack()->updateStatus($this, $params);

            $view_data = ['params' => $params];

            if (is_a($this, Container::class)) {
                $view_data['slot'] = $this->getSlot();
            }

            foreach($this->bypassableAttributes() as $bypass) {
                $view_data[$bypass] = $data['attributes']->get($bypass);
            }

            foreach($this->fixedAttributes() as $key => $value) {
                $view_data[$key] = $value;
            }

            foreach($this->exposedMethods() as $method) {
                $view_data[$method] = $data[$method];
            }

            $ret = view('larastrap::' . $this->templateName(), $view_data)->render();
            $this->getStack()->popStatus();

            return $ret;
        };
    }

    protected function baseClass()
    {
        return '';
    }

    protected function serializableAttributes()
    {
        return (object) [
            'literals' => [],
            'booleans' => ['hidden'],
        ];
    }

    /*
        Array of attributes to be just passed from the user's template
        definition to the component's template
    */
    protected function bypassableAttributes()
    {
        return [];
    }

    /*
        Associative array of fixed attributes names and values that the
        component wants. Used for extending existing components and enforce the
        desired behaviour
    */
    protected function fixedAttributes()
    {
        return [];
    }

    /*
        The returned array is a list of methods defined within the class, which
        have to be accessible from the template file.
        As in native Component Methods
        https://laravel.com/docs/8.x/blade#component-methods
        but to be explicitely handled, in render(), due the usage of a different
        rendering flow
    */
    protected function exposedMethods()
    {
        return [];
    }

    /*
        Array of attributes valid only for the current component, not to be
        inherited from the parents
    */
    protected function localAttributes()
    {
        return ['id', 'classes', 'size', 'value', 'options', 'label', 'help', 'pophelp', 'reviewCallback'];
    }

    /*
        Array of attributes which values have to be merged across global and
        local configuration. Only arrays are accepted
    */
    protected function mergiableAttributes()
    {
        return ['classes', 'attributes', 'label_class'];
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function closestParent($target_class)
    {
        $parent = $this;

        do {
            $parent = $parent->getParent();
            if (is_a($parent, $target_class)) {
                return $parent;
            }
        } while(is_null($parent) == false);

        return null;
    }

    public static function parameters()
    {
        return [
            'id' => (object) [
                'type' => 'string',
                'default' => null,
            ],
            'attributes' => (object) [
                'type' => 'index_array',
                'default' => [],
            ],
            'classes' => (object) [
                'type' => 'string_array',
                'default' => [],
            ],
            'override_classes' => (object) [
                'type' => 'string_array',
                'default' => [],
            ],
            'hidden' => (object) [
                'type' => 'boolean',
                'default' => false,
            ],
            'reviewCallback' => (object) [
                'type' => 'callable',
                'default' => null,
            ],
        ];
    }

    public function defaults()
    {
        $ret = [];

        foreach(static::parameters() as $key => $meta) {
            $ret[$key] = $meta->default;
        }

        return $ret;
    }

    public function nodeName()
    {
        if (is_null($this->nodename)) {
            $classname = get_class($this);
            $this->nodename = strtolower(substr($classname, strrpos($classname, '\\') + 1));
        }

        return $this->nodename;
    }

    public function templateName()
    {
        return $this->nodeName();
    }

    protected function getStack()
    {
        if (is_null($this->stack)) {
            $this->stack = app()->make('LarastrapStack');
        }

        return $this->stack;
    }

    /*
        Note: using a totally random ID, the final view will never be cached by
        native Laravel's system and will produce tons of useless cache files.
        Here we generate a unique identifier based on the value of mostly unique
        parameters across elements for the instance of the object: it is the
        same within different iterations only in certain conditions, but at
        least will mitigate proliferation of different cache elements.
    */
    private function uniqueId($params)
    {
        if (is_null($this->unique_id)) {
            $contents = '';

            $keys = ['id', 'name', 'label', 'label_html', 'help', 'pophelp', 'value', 'classes', 'action', 'title'];
            foreach($keys as $k) {
                $p = $params[$k] ?? '';

                if (is_array($p)) {
                    $p = join('', $p);
                }

                if (is_string($p) == false) {
                    continue;
                }

                $contents .= $p;
            }

            $stack = $this->getStack();

            if ($this->parent) {
                $contents .= $this->parent->uniqueId($stack->getStatus($this->parent, false));
            }

            /*
                To provide as much "unique" criteria for ID generation, here
                also the total size of the current stack is included. This is to
                tackle conditions in which similar elements (with same label and
                parameters) appear within the same page
            */
            $contents .= (string) $stack->getCounter();

            if (empty($contents)) {
                $this->unique_id = Str::random(20);
            }
            else {
                $this->unique_id = md5($contents);
            }
        }

        return $this->unique_id;
    }

    protected function processParams($params)
    {
        if (is_null($params['id'] ?? null)) {
            $nodename = $this->templateName();
            $params['id'] = Str::slug($nodename) . '-' . $this->uniqueId($params);
        }
        else {
            $params = array_merge($params, $this->getStack()->getPendingByID($params['id']));
        }

        foreach(static::parameters() as $key => $meta) {
            if (isset($meta->translates)) {
                $params = Commons::retrieveText($params, $meta->translates, $key);
            }
        }

        return $params;
    }

    /*
        This is called by generateParams() only at the final stage (when
        rendering the element)
    */
    protected function finalProcess($params)
    {
        if (!empty($params['override_classes'])) {
            $params['classes'] = $params['override_classes'];
        }

        return $params;
    }

    private function lastUserTouch($params)
    {
        if (is_callable($params['reviewCallback'])) {
            $params = call_user_func($params['reviewCallback'], $this, $params);
        }

        return $params;
    }

    /*
        For Containers, this is actually called twice: in the Stack, when I'm
        getting the status of parent nodes to inherit attributes, and at the
        moment of the actual render().
        The attributes may change meanwhile (may have been changed by a child),
        so it is required to handle them before and after. But the second case
        is considered "final", as the rendering stage is the last one valid to
        make elaboration.

        TODO: provide some optimization to avoid execute twice the whole process
        if nothing have changed.
    */
    public function generateParams($final)
    {
        $nodename = $this->nodeName();
        $stack = $this->getStack();

        $default_params = $this->defaults();
        $globalconf_params = $stack->getCommonsConfig();
        $nodeconf_params = $stack->getNodeConfig($nodename);

        /*
            The parameters passed inline to the component. Note: those are
            accessible only when inside the callback returned by render().
            For convenience, those can be passed both as individual attributes
            or into an associative array named "params"
        */
        $inline_params = [
            'attributes' => [],
        ];

        $parameters = static::parameters();
        $my_parameters = array_keys($parameters);

        foreach($this->data()['attributes'] as $key => $value) {
            if ($key == 'params') {
                foreach($value as $subkey => $subvalue) {
                    $inline_params[$subkey] = $subvalue;
                }
            }
            elseif (in_array($key, $my_parameters) == false) {
                $inline_params['attributes'][$key] = $value;
            }
            else {
                $inline_params[$key] = $value;
            }
        }

        $inline_params = Commons::normalizeParams($inline_params, $parameters);

        /*
            The parameters from the parent component, to enable inheritance
            (e.g. to pass the "view" property from a form to his inner fields)
        */
        if ($this->parent) {
            $parent_params = $stack->getStatus($this->parent);

            foreach($this->localAttributes() as $local) {
                unset($parent_params[$local]);
            }
        }
        else {
            $parent_params = [];
        }

        /*
            If the current component is invoked as a custom component, here I
            retrieve the proper preset defined in configuration to be merged
            into the local set of attributes
        */
        $custom_params = [];
        $actual_component_name = $this->data()['componentName'];
        $my_component_name = sprintf('larastrap::' . $this->templateName());
        if ($actual_component_name != $my_component_name) {
            $tagname = substr($actual_component_name, strrpos($actual_component_name, ':') + 1);
            $custom_params = $stack->getCustomConfig($tagname);
        }

        /*
            I have to retrieve the status eventually modified from children
            components, which may alter the behaviour of the parent node
            (e.g. a File component enforces a enctype attribute to the closest
            Form)
        */
        $altered_params = $stack->getStatus($this, false);

        foreach($this->mergiableAttributes() as $mergiable) {
            $merged = array_merge(
                $default_params[$mergiable] ?? [],
                $globalconf_params[$mergiable] ?? [],
                $nodeconf_params[$mergiable] ?? [],
                $parent_params[$mergiable] ?? [],
                $custom_params[$mergiable] ?? [],
                $inline_params[$mergiable] ?? [],
                $altered_params[$mergiable] ?? [],
            );

            $inline_params[$mergiable] = $merged;
        }

        /*
            All the configurations are here merged in the one to be actually
            used. The order of merging arrays is important!
        */
        $glued = array_merge(
            $default_params,
            $globalconf_params,
            $nodeconf_params,
            $parent_params,
            $custom_params,
            $altered_params,
            $inline_params,
        );

        $glued['classes'][] = $this->baseClass();

        $glued = $this->processParams($glued);
        $glued = $this->lastUserTouch($glued);

        if ($final) {
            $glued = $this->finalProcess($glued);
        }

        $glued['generated_class'] = join(' ', array_unique($glued['classes']));
        $glued['serialized_attributes'] = $this->getSerializedAttributes($glued);
        return $glued;
    }

    private function getSerializedAttributes($params)
    {
        $serializable = $this->serializableAttributes();
        $serialized_attributes = [];

        foreach($serializable->literals as $lit) {
            if (filled($params[$lit] ?? '')) {
                $serialized_attributes[] = sprintf('%s="%s"', $lit, $params[$lit]);
            }
        }

        foreach($serializable->booleans as $bool) {
            if ($params[$bool] ?? false) {
                $serialized_attributes[] = $bool;
            }
        }

        return Commons::serializeAttributes($params['attributes'], $serialized_attributes);
    }

    /*
        The original extractBladeViewFromString() from Illuminate\View\Component
        search for any template file named as $contents before actually handling
        it as a Blade template.
        As $contents here is always a Blade string, the function is overridden
        to completely skip to (slow) filesystem lookup
    */
    protected function extractBladeViewFromString($contents)
    {
        $key = sprintf('%s::%s', static::class, $contents);

        if (isset(static::$bladeViewCache[$key])) {
            return static::$bladeViewCache[$key];
        }

        return static::$bladeViewCache[$key] = $this->createBladeViewFromString($this->factory(), $contents);
    }
}
