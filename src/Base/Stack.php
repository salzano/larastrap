<?php

namespace MadBob\Larastrap\Base;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Str;

use HaydenPierce\ClassFinder\ClassFinder;

class Stack
{
    private $elements_stack = [];
    private $status_stack = [];

    private $all_params = [];
    private $commons_configs = [];
    private $commons_configs_backup = [];
    private $nodes_configs = [];
    private $nodes_configs_backup = [];

    private $custom_configs = [];
    private $travelling_configs = [];
    private $total_count = 0;

    public function __construct()
    {
        $components = ClassFinder::getClassesInNamespace('MadBob\Larastrap\Components');
        foreach($components as $component) {
            $this->all_params = array_merge($this->all_params, $component::parameters());
        }

        $this->commons_configs = config('larastrap.commons', []);
        $this->commons_configs = Commons::normalizeParams($this->commons_configs, $this->all_params);

        $this->nodes_configs = config('larastrap.elements', []);
        foreach($this->nodes_configs as $node => $par) {
            $this->nodes_configs[$node] = Commons::normalizeParams($par, $this->all_params);
        }

        $this->commons_configs_backup = $this->commons_configs;
        $this->nodes_configs_backup = $this->nodes_configs;

        $this->pushStatus(null);
    }

    public function addCustomElement($tag, $config)
    {
        $parentclass = 'MadBob\\Larastrap\\Components\\' . Str::ucfirst(Str::camel($config['extends']));
        Blade::component('larastrap::' . $tag, $parentclass);
        $this->custom_configs[$tag] = $config['params'];
    }

    public function getCommonsConfig()
    {
        return $this->commons_configs ?? [];
    }

    public function getNodeConfig($tag)
    {
        return $this->nodes_configs[$tag] ?? [];
    }

    /*
        This function is intended to be publicly used to override the
        configuration usually get from the config/larastrap.php file
    */
    public function overrideNodesConfig($config = null)
    {
        if (is_null($config)) {
            $this->nodes_configs = $this->nodes_configs_backup;
        }
        else {
            foreach($config as $node => $c) {
                $config[$node] = Commons::normalizeParams($config[$node], $this->all_params);
            }

            $this->nodes_configs = array_merge($this->nodes_configs, $config);
        }
    }

    public function getCustomConfig($tag)
    {
        return $this->custom_configs[$tag] ?? [];
    }

    public function getCurrentContainer()
    {
        for ($index = count($this->elements_stack) - 1; $index > 0; $index--) {
            if (is_a($this->elements_stack[$index], Container::class)) {
                return $this->elements_stack[$index];
            }
        }

        return null;
    }

    /*
        Note: this function returns the total count of elements pushed into the
        Stack, not his current size. It is intended to be a global counter,
        useful in particular when handling unique IDs generation
    */
    public function getCounter()
    {
        return $this->total_count;
    }

    public function getStatus($target = null, $populate = true)
    {
        if (is_null($target)) {
            return $this->status_stack[count($this->status_stack) - 1];
        }
        else {
            foreach($this->elements_stack as $index => $iter) {
                if ($iter == $target) {
                    if (empty($this->status_stack[$index]) && $populate) {
                        $this->status_stack[$index] = $target->generateParams(false);
                    }

                    return $this->status_stack[$index];
                }
            }
        }

        return [];
    }

    public function pushStatus($obj)
    {
        $this->elements_stack[] = $obj;
        $this->status_stack[] = [];
        $this->total_count++;
    }

    public function updateStatus($obj, $configs)
    {
        foreach($this->elements_stack as $index => $iter) {
            if ($iter == $obj) {
                $this->status_stack[$index] = array_merge($this->status_stack[$index], $configs);
                break;
            }
        }
    }

    public function popStatus()
    {
        $obj = array_pop($this->elements_stack);
        return array_pop($this->status_stack);
    }

    public function getLocalValue($name, $params)
    {
        $obj = $params['obj'] ?? null;
        if ($obj) {
            return $obj->$name ?? null;
        }
        else {
            return null;
        }
    }

    public function getValue($name)
    {
        for ($index = count($this->status_stack) - 1; $index > 0; $index--) {
            $value = $this->getLocalValue($name, $this->status_stack[$index]);
            if ($value) {
                return $value;
            }
        }

        return null;
    }

    public function getPendingByID($id)
    {
        return $this->travelling_configs[$id] ?? [];
    }

    public function setStatusByID($id, $configs)
    {
        foreach($this->elements_stack as $index => $iter) {
            $iid = $this->status_stack[$index]['id'] ?? null;
            if ($iid == $id) {
                $this->updateStatus($iter, $configs);
                return;
            }
        }

        $this->travelling_configs[$id] = $configs;
    }
}
